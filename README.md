# Teste do Caixa

Este é um programa escrito exclusivamente para o teste da BRQ. Este programa foi desenvolvido usando JavaScript com NodeJS (npm).

Para começar a avaliar, siga os passos a seguir:

## 1 - Clonar ou baixar o repositório

## 2 - Preparar o ambiente

Para isso, executar os comandos:

```
npm install
```

## 3 - Executar o programa

Para isso, executar os comandos:

```
npm start
```

E inserir os montantes desejados para sacar.

## 4 - Testes

Foi utilizado uma estrutura BDD (TDD), e os testes encontram-se em um arquivo dedicado a eles. Para realizar os testes, executar os comandos:

```
npm test
```

