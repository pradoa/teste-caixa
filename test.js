var should = require('should');
var caixa = require('./caixa')

describe('Testando Saques', function () {
    it('should return unable to withdraw if value < 10', function () {
        caixa.Sacar(9).should.be.equal(-1);
    });

    it('should return unable to withdraw if value is not multiple of 10', function () {
        caixa.Sacar(95).should.be.equal(-1);
    });

    it('should return 1 R$20,00 and 1 R$30,00 if value = 30', function () {
        caixa.Sacar(30).should.be.equal('Entregar 1 nota(s) de R$20,00 1 nota(s) de R$10,00 ');
    });

    it('should return 1 R$50,00 and 1 R$20,00 and 1 R$10,00 if value = 80', function () {
        caixa.Sacar(80).should.be.equal('Entregar 1 nota(s) de R$50,00 1 nota(s) de R$20,00 1 nota(s) de R$10,00 ');
    });
});