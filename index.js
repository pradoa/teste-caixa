var readline = require('readline');
var caixa = require('./caixa')

var rl = readline.createInterface(process.stdin, process.stdout);
rl.setPrompt('Insira o valor do Saque > ');
rl.prompt();
rl.on('line', function (line) {
    let val = Number.parseInt(line, 10)
    caixa.Sacar(val)
    rl.prompt();
}).on('close', function () {
    process.exit(0);
});
