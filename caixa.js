let Caixa = {}

Caixa.Notas = [
    100,
    50,
    20,
    10
]

Caixa.Sacar = function (valor) {
    if (valor < 10 || valor % 10 !== 0) {
        console.log(`Não há notas disponíveis para realizar este saque.`)
        return -1
    }

    let valorSacado = 0
    let notas = [0, 0, 0, 0]

    while (valorSacado < valor) {
        for (let nota = 0; nota < notas.length; nota++) {
            if (valorSacado + this.Notas[nota] <= valor) {
                notas[nota] += 1
                valorSacado += this.Notas[nota]
                break
            }
        }
    }

    let output = `Entregar `
    for (let i = 0; i < notas.length; i++) {
        if (notas[i] > 0)
            output += `${notas[i]} nota(s) de R$${this.Notas[i]},00 `
    }

    console.log(output)
    return output
}

module.exports = Caixa